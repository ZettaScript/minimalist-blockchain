# Python Simple Blockchain

This is a full-Python cryptomonetary blockchain pedagogical project.

## Project structure

* **apply.py**: applies a block to the blockchain and indexes
* **block.py**: defines block and document data structures and encoding/decoding functions
* **blockchain.py**: manages blockchain storage, reading and writing
* **client.py**: CLI client for using blockchain
* **constants.py**: defines global constants
* **global_valid.py**: performs global validation of a block
* **indexes.py**: manages index storage, reading and writing
* **network.py**: handles server and requests
* **node.py**: server entry point
* **pool.py**: manages document and block pool (checks and stores data received from network)
* **pow.py**: performs Proof of Work (PoW) on a given block
* **utils.py**: defines some useful generic functions

* **uncomplete/**: contains uncomplete source code, in order to by completed by trainees

## Install

Install python3 and pip ([instructions here](https://python.org)).

Install packages varint, cbor, duniterpy, linbacl, base58:

	sudo pip3 install --upgrade varint cbor duniterpy libnacl base58

## Use

`peers.txt` is the list of known nodes. Put one node per line with format `hostname:port`. New nodes will automatically be added at the end of the file.

	python3 node.py --help
	
	# Start node & create new blockchain
	python3 node.py clean genesis start -h <public-host>
	
	# Start node & use existing blockchain or sync with nodes
	python3 node.py start
