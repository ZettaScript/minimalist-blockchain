#!/usr/bin/env python3

from constants import *
from block import Block, BlockWrapper
import utils

import time
from threading import Thread

class PowThread(Thread):
	def __init__(self, block, is_valid, is_valid_args, privkey, cpu=POW_CPU, hash_func=HASH_FUNC):
		Thread.__init__(self)
		
		self.bw = BlockWrapper(block, None)
		self.is_valid = is_valid
		self.is_valid_args = is_valid_args
		self.privkey = privkey
		self.cpu = cpu
		self.hash_func = hash_func
		
		self.loop = True
		self.done = False
	
	def run(self):
		sleepfactor = (1-self.cpu)/self.cpu
		while self.loop:
			t = time.time()
			
			self.bw.update(self.bw.block, self.privkey)
			
			if self.is_valid(self.bw.hash, *self.is_valid_args):
				self.done = True
				self.loop = False
			else:
				self.bw.block.nonce += 1
			
			time.sleep((time.time() - t) * sleepfactor)

class Pow:
	def __init__(self, privkey, diffi=POW_DIFFICULTY):
		self.privkey = privkey
		self.diffi = diffi
		
		self.thread = None
	
	def start(self, block):
		# Stop old thread
		self.stop()
		
		# Start new thread
		self.thread = PowThread(block, utils.is_valid_zeros, [self.diffi], self.privkey)
		self.thread.start()
		print("Pow: Started {}".format(block.number))
	
	def get_state(self):
		if self.thread:
			if self.thread.done:
				self.thread.join()
				bw = self.thread.bw
				self.thread = None
				print("Pow: Found {}".format(bw.human_readable_id()))
				return bw
		return None
	
	def wait(self, interval=0.01):
		if not self.thread:
			return None
		while True:
			if self.thread.done:
				self.thread.join()
				bw = self.thread.bw
				self.thread = None
				print("Pow: Found {}".format(bw.human_readable_id()))
				return bw
			time.sleep(interval)
	
	def stop(self):
		if self.thread:
			self.thread.loop = False
			self.thread.join()
			self.thread = None
		