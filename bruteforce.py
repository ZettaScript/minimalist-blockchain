from duniterpy.key import SigningKey
import sys,time

ALPHABET = "abzertyuiopqsdfghjklmwxcvn123456789"
test = "1234"


def loopb(n,B,A,pk):
	if n>0:
		for j in ALPHABET:
			loopb(n-1,B+j,A,pk)
	else:
		key = SigningKey.from_credentials(A,B).pubkey
		print(A,B,end=' '*len(A)+"\r")
		if key==pk:
			print("Find [{}] [{}] for \n {}".format(A,B,pk))
			exit()

def loopa(start,n,A,pk):
	for i in ALPHABET:
		if n>0:
			loopa(start,n-1,A+i,pk)
		else:
			loopb(start,"",A,pk)
			break

		
if __name__ == "__main__":
	try:
		n=int(sys.argv[1])
		pk=sys.argv[2]
	except:exit()
	
	loopa(n,n,"",pk)
	
	#1D
	#for i in ALPHABET:
	#	for j in ALPHABET:
	#		key = SigningKey.from_credentials(i,j).pubkey
	#		if key==pk:
	#			print("Found {0},{1} for \n {2}".format(i,j,key))
	#			exit()
	
	