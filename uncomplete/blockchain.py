#!/usr/bin/env python3

from constants import *
from block import Block, BlockWrapper

import varint

class Blockchain:
	def __init__(self, filename=BLOCKCHAIN_FILENAME):
		self.filename = filename
		
		self.block_index = []
		self.hash_index = {}
	
	def load(self):
		# Clean indexes
		self.block_index = []
		self.hash_index = {}
		
		# Open file
		stream = open(self.filename, "rb")
		while True:
			
			# Decode data
			try:
				length = varint.decode_stream(stream)
			except TypeError:
				break
			raw = stream.read(length)
			bw = BlockWrapper.from_raw(raw)
			
			# Index
			self.block_index.append(bw)
			self.hash_index[bw.hash] = bw.block.number
		
		if len(self.block_index) > 0:
			print("Loaded blockchain:", bw.human_readable_id())
		else:
			print("Loaded blockchain: empty")
	
	def save(self):
		# Open file
		stream = open(self.filename, "wb")
		for bw in self.block_index:
			
			# Write data
			stream.write(varint.encode(len(bw.raw)))
			stream.write(bw.raw)
		
		print("Saved blockchain")
	
	def add_block(self, bw):
		self.block_index.append(bw)
		self.hash_index[bw.hash] = bw.block.number
	
	def remove_last_block(self):
		bw = self.block_index.pop()
		self.hash_index.pop(bw.hash)
		return bw
