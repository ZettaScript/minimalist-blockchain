#!/usr/bin/env python3

from constants import *
from block import BlockWrapper, TxWrapper
import global_valid, apply

import io, socket, time, cbor, varint
from threading import Thread

STEP_LENGTH = 0
STEP_CONTENT = 1

REQTYPE_GETBLOCKSBYID = 1
REQTYPE_NEWBLOCKS = 2
REQTYPE_NEWDOCS = 3
REQTYPE_GETBLOCKCOUNT = 4
REQTYPE_GETBLOCKHASHES = 5
REQTYPE_GETBLOCKSBYHASH = 6
REQTYPE_HELLO = 7
REQTYPE_GETAMOUNTS = 8
REQTYPE_GETSOURCES = 9

def read_packet(client):
	step = STEP_LENGTH
	length = None
	recv_length = 0
	data = b""
	i = 1
	
	# Receive data
	while True:
		try:
			raw = client.recv(NETWORK_BUFFER)
		except socket.timeout:
			break
		if len(raw) == 0:
			break
		recv_length += len(raw)
		data += raw
		
		if step == STEP_LENGTH:
			i = 1
			for c in raw:
				if c < 0x80:
					step = STEP_CONTENT
					length = varint.decode_bytes(data[:i])
					recv_length -= i
					break
				i += 1
		if step != STEP_LENGTH:
			if recv_length >= length or recv_length > NETWORK_MAXLENGTH:
				break
	
	#print("Received ", data[i:])
	return io.BytesIO(data[i:])

def send_req(host, raw, resp=True):
	#print("Send", raw)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(NETWORK_TIMEOUT)
	sock.connect(host)
	
	sock.sendall(varint.encode(len(raw)) + raw)
	
	if resp:
		return read_packet(sock)

def send_response(client, raw):
	try:
		client.sendall(varint.encode(len(raw)) + raw)
	except OSError:
		pass
	client.close()

class Server(Thread):
	def __init__(self, blockchain, indexes, pool, peers, privkey, host=NETWORK_HOST):
		Thread.__init__(self)
		
		self.blockchain = blockchain
		self.indexes = indexes
		self.pool = pool
		self.peers = peers
		self.privkey = privkey
		self.host = host
		
		self.loop = True
		self.sock = None
	
	def run(self):
		
		# Init server
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.settimeout(NETWORK_TIMEOUT)
		self.sock.bind(self.host)
		self.sock.listen(1)
		print("Server listening on {}:{}".format(*self.host))
		
		# Server loop
		while self.loop:
			try:
				client, addr = self.sock.accept()
			except socket.timeout:
				continue
			
			data = read_packet(client)
			
			# Get request type
			reqtype = varint.decode_stream(data)
			
			if reqtype == REQTYPE_GETBLOCKSBYID:
				#print("Req: getblocksbyid")
				block_ids = cbor.load(data)
				blocks = []
				i = 0
				for block_id in block_ids:
					if len(self.blockchain.block_index) > block_id:
						blocks.append(self.blockchain.block_index[block_id].raw)
						i += 1
						if i >= NETWORK_GETBLOCKS_MAXBLOCKS:
							break
				
				send_response(client, cbor.dumps(blocks))
			
			elif reqtype == REQTYPE_NEWBLOCKS:
				#print("Req: newblocks")
				new_blocks = cbor.load(data)
				for new_block in new_blocks:
					bw = BlockWrapper.from_raw(new_block)
					if not bw.hash in self.pool.blocks_unchecked and not bw.hash in self.pool.blocks:
						with self.pool.blocks_unchecked_lock:
							self.pool.blocks_unchecked[bw.hash] = bw
			
			elif reqtype == REQTYPE_NEWDOCS:
				#print("Req: newdocs")
				new_docs = cbor.load(data)
				for new_doc in new_docs:
					docw = TxWrapper.from_raw(new_doc)
					if not docw.hash in self.pool.docs_unchecked and not docw.hash in self.pool.docs:
						with self.pool.docs_unchecked_lock:
							self.pool.docs_unchecked[docw.hash] = docw
			
			elif reqtype == REQTYPE_GETBLOCKCOUNT:
				#print("Req: getblockcount")
				send_response(client, varint.encode(len(self.blockchain.block_index)))
			
			elif reqtype == REQTYPE_GETBLOCKHASHES:
				#print("Req: getblockhashes")
				nb_from = varint.decode_stream(data)
				
				result = [bw.hash for bw in self.blockchain.block_index[nb_from:]]
				for hash in self.pool.blocks:
					if self.pool.blocks[hash].block.number >= nb_from:
						result.append(self.pool.blocks[hash].raw)
				
				send_response(client,
					varint.encode(max(0, len(self.blockchain.block_index) - nb_from)) +\
					b"".join(result)
				)
			
			elif reqtype == REQTYPE_GETBLOCKSBYHASH:
				#print("Req: getblocksbyhash")
				nb_hashes = varint.decode_stream(data)
				result = []
				k = 0
				for i in range(nb_hashes):
					hash = data.read(HASH_SIZE)
					if hash in self.blockchain.hash_index:
						result.append(self.blockchain.block_index[self.blockchain.hash_index[hash]].raw)
						k += 1
						if k > NETWORK_GETBLOCKS_MAXBLOCKS:
							break
					elif hash in self.pool.blocks:
						result.append(self.pool.blocks[hash].raw)
						k += 1
						if k > NETWORK_GETBLOCKS_MAXBLOCKS:
							break
				send_response(client, cbor.dumps(result))
			
			elif reqtype == REQTYPE_HELLO:
				#print("Req: hello")
				new_peers = cbor.load(data)
				for new_peer in new_peers:
					peer = tuple(new_peer)
					if not peer in self.peers and peer != self.host:
						print("Network: new peer", peer)
						self.peers.append(peer)
				send_response(client, cbor.dumps([self.host] + self.peers))
			
			elif reqtype == REQTYPE_GETAMOUNTS:
				#print("Req: getamounts")
				pubkeys = cbor.load(pubkeys)
				result = {}
				for pubkey in pubkeys:
					result[pubkey] = self.indexes.pubkeys[pubkey]["amount"] if pubkey in self.indexes.pubkeys else 0
				send_response(client, cbor.dumps(result))
			
			elif reqtype == REQTYPE_GETSOURCES:
				#print("Req: getsources")
				pubkeys = cbor.load(pubkeys)
				result = {}
				for pubkey in pubkeys:
					if pubkey in result:
						continue
					sources = {}
					if pubkey in self.indexes.pubkeys:
						for hash in self.indexes.sources:
							if self.indexes.sources[hash][0] == pubkey:
								sources[hash] = self.indexes.sources[hash][1]
					result[pubkey] = sources
				send_response(client, cbor.dumps(result))
			
			send_response(client, b"")
		
		self.sock.close()

class Client(Thread):
	def __init__(self, blockchain, indexes, pool, peers, privkey, host=NETWORK_HOST):
		Thread.__init__(self)
		
		self.blockchain = blockchain
		self.indexes = indexes
		self.pool = pool
		self.peers = peers
		self.privkey = privkey
		self.host = host
		
		self.loop = True
	
	def spread_blocks(self, block_ids):
		print("Network: Spread {}".format(block_ids))
		for peer in self.peers:
			try:
				send_req(
					peer,
					varint.encode(REQTYPE_NEWBLOCKS) +\
					cbor.dumps([self.blockchain.block_index[i].raw for i in block_ids]),
					False
				)
			except ConnectionRefusedError:
				pass
	
	def get_blocks(self):
		print("Network: Getting blocks")
		
		first_number = max(0, len(self.blockchain.block_index)-1-FORK_MAX_REVERT)
		
		# Fetch blocks we don't have and which are in fork window
		loop = True
		while loop:
			loop = False
			for peer in self.peers:
				try:
					# Get block hashes
					data = send_req(peer, varint.encode(REQTYPE_GETBLOCKHASHES) + varint.encode(first_number))
					
					hashes = []
					for i in range(varint.decode_stream(data)):
						hash = data.read(HASH_SIZE)
						if not hash in self.pool.blocks_unchecked and not hash in self.pool.blocks and not hash in self.blockchain.hash_index:
							hashes.append(hash)
					
					if len(hashes) > 0:
						# Fetch blocks
						data = send_req(peer, varint.encode(REQTYPE_GETBLOCKSBYHASH) + varint.encode(len(hashes)) + b"".join([i for i in hashes]))
						new_blocks = cbor.load(data)
						with self.pool.blocks_unchecked_lock:
							for new_block in new_blocks:
								bw = BlockWrapper.from_raw(new_block)
								self.pool.blocks_unchecked[bw.hash] = bw
								loop = True
				
				except ConnectionRefusedError:
					pass
	
	def spread_docs(self):
		pass
	
	def get_genesis_block(self):
		print("Network: Getting genesis block")
		
		for peer in self.peers:
			try:
				data = send_req(peer, varint.encode(REQTYPE_GETBLOCKSBYID) + cbor.dumps([0]))
				new_blocks = cbor.load(data)
				bw = BlockWrapper.from_raw(new_blocks[0])
				
				assert bw.check_local_validity()
				assert global_valid.check_global_validity(self.blockchain, self.indexes, bw)
				apply.apply_block(self.blockchain, self.indexes, bw)
				
				break
			except (ConnectionRefusedError, AssertionError):
				pass
	
	def say_hello(self):
		print("Network: Saying hello")
		
		for peer in self.peers:
			try:
				data = send_req(peer, varint.encode(REQTYPE_HELLO) + cbor.dumps([self.host] + self.peers))
				
				new_peers = cbor.load(data)
				for new_peer in new_peers:
					peer = tuple(new_peer)
					if not peer in self.peers and peer != self.host:
						print("Network: new peer", peer)
						self.peers.append(peer)
			
			except ConnectionRefusedError:
				pass
	
	def run(self):
		next_getblocks = 0
		next_hello = 0
		nb_blocks = len(self.blockchain.block_index)
		
		# Client loop
		while self.loop:
			
			if time.time() > next_hello:
				self.say_hello()
				next_hello = time.time() + NETWORK_HELLO_INTERVAL
			
			if time.time() > next_getblocks:
				self.get_blocks()
				next_getblocks = time.time() + NETWORK_GETBLOCKS_INTERVAL
			
			if len(self.blockchain.block_index) > nb_blocks:
				self.spread_blocks(list(range(max(nb_blocks, len(self.blockchain.block_index)-NETWORK_GETBLOCKS_MAXBLOCKS), len(self.blockchain.block_index))))
				nb_blocks = len(self.blockchain.block_index)
			
			time.sleep(CLIENT_LOOP_SLEEP)
