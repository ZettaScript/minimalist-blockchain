#!/usr/bin/env python3

from constants import *
import utils

import varint, cbor, base58
from io import BytesIO

class TxInput:
	def __init__(self, amount, hash):
		self.amount = amount
		self.hash = hash
	
	def to_raw(self):
		return self.hash + varint.encode(self.amount)
	
	def from_raw(raw):
		return TxInput(varint.decode_bytes(raw[HASH_SIZE:]), raw[:HASH_SIZE])
	
	def list_from_stream(stream):
		result = []
		for i in range(varint.decode_stream(stream)):
			result.append(TxInput(
				hash = stream.read(HASH_SIZE),
				amount = varint.decode_stream(stream)
			))
		return result
	
	def list_to_raw(txis):
		raw = varint.encode(len(txis))
		for txi in txis:
			raw += txi.hash + varint.encode(txi.amount)
		return raw
	
	def to_tuple(self):
		return [self.hash, self.amount]
	
	def from_tuple(val):
		return TxInput(val[1], val[0])

class TxOutput:
	def __init__(self, amount, pubkey):
		self.amount = amount
		self.pubkey = pubkey
	
	def to_raw(self):
		return self.pubkey + varint.encode(self.amount)
	
	def from_raw(raw):
		return TxOutput(varint.decode_bytes(raw[PUBKEY_SIZE:]), raw[:PUBKEY_SIZE])
	
	def list_from_stream(stream):
		result = []
		for i in range(varint.decode_stream(stream)):
			result.append(TxOutput(
				pubkey = stream.read(PUBKEY_SIZE),
				amount = varint.decode_stream(stream)
			))
		return result
	
	def list_to_raw(txos):
		raw = varint.encode(len(txos))
		for txo in txos:
			raw += txo.pubkey + varint.encode(txo.amount)
		return raw
	
	def to_tuple(self):
		return [self.pubkey, self.amount]
	
	def from_tuple(val):
		return TxOutput(val[1], val[0])

class Tx:
	def __init__(self, amount, comment, inputs, issuer, outputs):
		self.comment = comment
		self.inputs = inputs
		self.issuer = issuer
		self.outputs = outputs
	
	def from_raw(raw):
		inner, pubkey = BytesIO(utils.verify_sign(raw))
		assert inner.read(2) == b"tx" , "Error: not a tx"
		return Tx(
			inputs = TxInput.list_from_stream(inner),
			outputs = TxOutput.list_from_stream(inner),
			comment = inner.read(TX_COMMENT_SIZE),
			issuer = pubkey
		)
	
	def to_raw(self, privkey):
		return b"tx" + privkey.vk + privkey.sign(
			TxInput.list_to_raw(self.inputs) +\
			TxOutput.list_to_raw(self.outputs) +\
			self.comment
		)

class TxWrapper:
	def __init__(self, tx, raw):
		self.tx = tx
		self.raw = raw
		
		self.hash = HASH_FUNC(raw) if raw else None
	
	def check_local_validity(self):
		True
	
	def from_raw(raw):
		return TxWrapper(
			Tx.from_raw(raw),
			raw
		)
	
	def from_tx(tx, privkey):
		return TxWrapper(
			tx,
			tx.to_raw(privkey)
		)

class Block:
	def __init__(self, content, issuer, number, previous_hash, nonce=0):
		self.content = content
		self.issuer = issuer
		self.number = number
		self.previous_hash = previous_hash
		self.nonce = nonce
	
	def from_raw(raw):
		inner, pubkey = utils.verify_sign(raw)
		stream = BytesIO(inner)
		return Block(
			number = varint.decode_stream(stream),
			nonce = varint.decode_stream(stream),
			previous_hash = stream.read(HASH_SIZE),
			content = Block.decode_content(stream.read()),
			issuer = pubkey
		)
	
	def to_raw(self, privkey):
		return privkey.vk + privkey.sign(
			varint.encode(self.number) +\
			varint.encode(self.nonce) +\
			self.previous_hash +\
			self.encode_content()
		)
	
	def copy(self):
		return Block(
			self.content,
			self.issuer,
			self.number,
			self.previous_hash,
			self.nonce
		)
	
	def decode_content(raw):
		# Decode content
		content = cbor.loads(raw)
		
		# Check type
		assert isinstance(content, dict) , "Invalid content"
		
		result = {}
		
		# Decode txs
		if "tx" in content:
			for tx in content["tx"]:
				result.append(TxWrapper.from_raw(tx))
		
		return result
	
	def encode_content(self):
		content = {}
		
		for doctype in self.content:
			content[doctype] = []
			for doc in self.content[doctype]:
				content[doctype].append(doc.raw)
		
		return cbor.dumps(content)

class BlockWrapper:
	def __init__(self, block, raw):
		self.block = block
		self.raw = raw
		
		self.hash = HASH_FUNC(raw) if raw else None
		
	def update(self, block, privkey):
		self.block = block
		self.raw = block.to_raw(privkey) if block else None
		self.hash = HASH_FUNC(self.raw) if block else None
	
	def check_local_validity(self):
		try:
			# Check format
			assert len(self.block.content) <= BLOCK_CONTENT_MAX_SIZE , "Content too long"
			assert len(self.block.issuer) == PUBKEY_SIZE , "Invalid issuer"
			assert isinstance(self.block.nonce, int) and self.block.nonce >= 0 , "Invalid nonce"
			assert isinstance(self.block.number, int) and self.block.number >= 0 , "Invalid number"
			assert len(self.block.previous_hash) == HASH_SIZE , "Invalid previous_hash"
			
			# Check difficulty
			assert utils.is_valid_zeros(self.hash, POW_DIFFICULTY) , "Invalid difficulty"
			
			# Check documents
			for doctype in self.block.content:
				for doc in self.block.content[doctype]:
					doc.check_local_validity()
		except AssertionError:
			return False
		
		return True
	
	def from_raw(raw):
		return BlockWrapper(
			Block.from_raw(raw),
			raw
		)
	
	def human_readable_id(self):
		return "{}-{}".format(self.block.number, base58.b58encode(self.hash)[:8].decode())
