#!/usr/bin/env python3

from constants import *

import cbor

class Indexes:
	def __init__(self, filename=INDEXES_FILENAME):
		self.filename = filename
		
		self.pubkeys = {}
		self.sources = {}
	
	def load(self):
		f = open(self.filename, "rb")
		indexes = cbor.load(f)
		f.close()
		self.pubkeys = indexes["pubkeys"]
		self.sources = indexes["sources"]
		
		print("Loaded indexes")
	
	def save(self):
		f = open(self.filename, "wb")
		f.write(cbor.dumps({
			"pubkeys": self.pubkeys,
			"sources": self.sources
		}))
		f.close()
		
		print("Saved indexes")
	
	def init_pubkey(self, pubkey, amount=0):
		self.pubkeys[pubkey] = {
			"amount": amount
		}
