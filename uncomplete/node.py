#!/usr/bin/env python3

from constants import *
import utils
import global_valid, apply
from block import Block, BlockWrapper
from blockchain import Blockchain
from indexes import Indexes
from network import Client, Server
from pool import Pool
from pow import Pow

import os, sys, time

if __name__ == "__main__":
	
	if "--help" in sys.argv or "help" in sys.argv:
		print("""Subcommands:
  clean    remove data files
  genesis  generate genesis block
  start    start node

Options:
  -h <host>  Default: ""
  -p <port>  Default: 58753
  -d <path>  Data directory. Default: "."
""")
		exit()
	
	# Get directory
	DIR = os.path.expanduser(utils.getargv("-d", "."))
	if DIR != "" and DIR[len(DIR)-1] == "/":
		DIR = DIR[:len(DIR)-1] # Remove last slash
	os.makedirs(DIR, exist_ok=True)
	
	BLOCKCHAIN_FILENAME = "{}/{}".format(DIR, BLOCKCHAIN_FILENAME)
	INDEXES_FILENAME = "{}/{}".format(DIR, INDEXES_FILENAME)
	PEERLIST_FILENAME = "{}/{}".format(DIR, PEERLIST_FILENAME)
	NETWORK_HOST = (utils.getargv("-h", NETWORK_HOST[0]), int(utils.getargv("-p", NETWORK_HOST[1])))
	
	if "clean" in sys.argv:
		try:
			os.remove(BLOCKCHAIN_FILENAME)
		except:
			pass
		try:
			os.remove(INDEXES_FILENAME)
		except:
			pass
	
	# Load data
	privkey = utils.prompt_privkey()
	blockchain = Blockchain(filename=BLOCKCHAIN_FILENAME)
	indexes = Indexes(filename=INDEXES_FILENAME)
	pow = Pow(privkey)
	pool = Pool(blockchain, indexes)
	peers = utils.read_peerlist(filename=PEERLIST_FILENAME)
	client = Client(blockchain, indexes, pool, peers, privkey, host=NETWORK_HOST)
	
	try:
		blockchain.load()
	except:
		print("Loaded blockchain: empty")
	try:
		indexes.load()
	except:
		print("Loaded indexes: empty")
	
	if "genesis" in sys.argv:
		# TODO Générer le bloc zéro (genesis)
	
	elif len(blockchain.block_index) == 0:
		client.get_genesis_block()
	
	if "start" in sys.argv:
		server = Server(blockchain, indexes, pool, peers, privkey, host=NETWORK_HOST)
		
		# TODO Boucle principale
	
	# Save data
	blockchain.save()
	indexes.save()
