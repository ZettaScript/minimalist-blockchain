#!/usr/bin/env python3

from constants import *
import global_valid, apply

from threading import RLock

class ForkTree:
	def __init__(self, parent=None):
		self.parent = parent
		
		self.branches = {}
		self.size = 0
	
	def add_branch(self, hash):
		if not hash in self.branches:
			self.branches[hash] = ForkTree(self)
			self.size = max(1, self.size)
			if self.parent:
				self.parent.update_size()
		return self.branches[hash]
	
	def update_size(self):
		for hash in self.branches:
			if self.branches[hash].size >= self.size:
				self.size = self.branches[hash].size + 1
		if self.parent:
			self.parent.update_size()
	
	def get_longest_branch(self):
		if len(self.branches) > 0:
			longest = max(self.branches, key=lambda hash: self.branches[hash].size)
			return [longest] + self.branches[longest].get_longest_branch()
		return []

class Pool:
	def __init__(self, blockchain, indexes):
		self.blockchain = blockchain
		self.indexes = indexes
		
		self.blocks = {}
		self.blocks_unchecked = {}
		self.blocks_unchecked_lock = RLock()
		self.docs = {}
		self.docs_lock = RLock()
		self.docs_unchecked = {}
		self.docs_unchecked_lock = RLock()
		self.docs_changed = False
	
	def update(self):
		first_number = max(0, len(self.blockchain.block_index)-1-FORK_MAX_REVERT)
		
		# Check unchecked blocks
		with self.blocks_unchecked_lock:
			for hash in self.blocks_unchecked:
				bw = self.blocks_unchecked[hash]
				
				# Check local validity and number
				if bw.check_local_validity() and bw.block.number >= first_number and not hash in self.blockchain.hash_index:
					self.blocks[hash] = bw
					print("Pool: add block", bw.human_readable_id())
			
			self.blocks_unchecked.clear()
		
		# Check pool blocks
		to_remove = []
		for hash in self.blocks:
			# Check number
			if self.blocks[hash].block.number < first_number or hash in self.blockchain.hash_index:
				print("Pool: remove block", self.blocks[hash].human_readable_id())
				to_remove.append(hash)
		
		for hash in to_remove:
			self.blocks.pop(hash)
		
		# Check unchecked docs
		with self.docs_unchecked_lock:
			for hash in self.docs_unchecked:
				txw = self.docs_unchecked[hash]
				
				if txw.check_local_validity() and not hash in self.docs:
					with self.docs_lock:
						self.docs[hash] = txw
			
			self.docs_unchecked.clear()
		
		# Check pool docs
		with self.docs_lock:
			to_remove = []
			for hash in self.docs:
				# Check if already applied
				found = False
				for bw in self.blockchain.block_index:
					if "tx" in bw.block.content:
						for txw in bw.block.content["tx"]:
							if txw.hash == hash:
								found = True
								break
						if found:
							break
				if found:
					to_remove.append(hash)
		
		for hash in to_remove:
			self.docs.pop(hash)
		
		if len(self.blocks) == 0:
			return
		
		# ------ Search for forks
		
		# Init fork tree
		first_bw = self.blockchain.block_index[first_number]
		fork_tree = ForkTree()
		fork_index = {first_bw.hash: fork_tree.add_branch(first_bw.hash)}
		
		# ---- Build fork tree
		
		# Add branches for stacked blocks
		for bw in self.blockchain.block_index[first_number+1:]:
			fork_index[bw.hash] = fork_index[bw.block.previous_hash].add_branch(bw.hash)
		
		# Add branches for pool blocks
		loop = True
		while loop:
			loop = False
			for hash in self.blocks:
				bw = self.blocks[hash]
				if not bw.block.previous_hash in fork_index:
					continue
				if not hash in fork_index:
					fork_index[hash] = fork_index[bw.block.previous_hash].add_branch(hash)
					loop = True
		
		# Get longest branch
		longest = fork_tree.get_longest_branch()
		
		# Find fork root
		fork_root = None
		i = 0
		for hash in longest:
			if hash in self.blockchain.hash_index:
				fork_root = hash
			else:
				break
			i += 1
		longest = longest[i:]
		
		# Revert blocks
		for i in range(len(self.blockchain.block_index) - 1 - self.blockchain.block_index[self.blockchain.hash_index[fork_root]].block.number):
			apply.revert_last_block(self.blockchain, self.indexes)
		
		# Apply blocks
		for hash in longest:
			if global_valid.check_global_validity(self.blockchain, self.indexes, self.blocks[hash]):
				apply.apply_block(self.blockchain, self.indexes, self.blocks[hash])
				self.blocks.pop(hash)
				self.docs_changed = True
			else:
				print("ERROR: invalid block in fork:", self.blocks[hash].human_readable_id())
		
		# TODO if invalid
