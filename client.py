#!/usr/bin/env python3

from constants import *
import utils
from block import Tx, TxWrapper, TxInput, TxOutput
import network

import sys, time, base58, cbor, varint

if __name__ == "__main__":
	
	if "--help" in sys.argv or "help" in sys.argv:
		print("""Subcommands:
  all               Get all pubkeys amounts
  amount <pubkey>   Get pubkey amount
  pk                Get pubkey from credentials
  tx <amount> <to>  Send tx

Options:
  -h <host>     Default: "127.0.0.1"
  -p <port>     Default: 58753
  -c <comment>  Comment for tx (default: "")
  -r            Refresh (for all)
""")
		exit()
	
	host = (utils.getargv("-h", "127.0.0.1"), int(utils.getargv("-p", 58753)))
	
	if "amount" in sys.argv:
		pubkey = base58.b58decode(utils.getargv("amount"))
		
		data = network.send_req(host, varint.encode(network.REQTYPE_GETAMOUNTS)+b"t" + cbor.dumps([pubkey]))
		
		print(cbor.load(data)[pubkey])
	
	elif "pk" in sys.argv:
		print(utils.prompt_privkey().pubkey)
	
	elif "tx" in sys.argv:
		amount = int(utils.getargv("tx"))
		comment = utils.getargv("-c","").encode()
		comment += bytes(TX_COMMENT_SIZE - len(comment))
		recipient = base58.b58decode(utils.getargv("tx", n=2))
		privkey = utils.prompt_privkey()
		
		data = network.send_req(host, varint.encode(network.REQTYPE_GETSOURCES) + cbor.dumps([privkey.vk]))
		sources = cbor.load(data)[privkey.vk]
		
		tx = Tx(comment, [], privkey.vk, [])
		
		i = 0
		for hash in sources:
			tx.inputs.append(TxInput(sources[hash], hash))
			i += sources[hash]
			if i >= amount:
				break
		print(i)
		
		if i < amount:
			print("Error: can't afford")
			exit(1)
		
		tx.outputs.append(TxOutput(amount, recipient))
		if i > amount:
			tx.outputs.append(TxOutput(i - amount, privkey.vk))
		
		raw = tx.to_raw(privkey)
		TxWrapper.from_raw(raw)
		
		network.send_req(host, varint.encode(network.REQTYPE_NEWDOCS) + cbor.dumps([tx.to_raw(privkey)]))
	
	elif "all" in sys.argv:
		def sub_all():
			try:
				data = network.send_req(host, varint.encode(network.REQTYPE_GETALLAMOUNTS))
			except:
				return
			pubkeys = cbor.load(data)
			
			print()
			for pubkey in pubkeys:
				print(base58.b58encode(pubkey).decode(), pubkeys[pubkey])
		
		if "-r" in sys.argv:
			while True:
				sub_all()
				time.sleep(2)
		else:
			sub_all()
