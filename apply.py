#!/usr/bin/env python3

from constants import *

import base58, varint

def apply_block(
	blockchain,
	indexes,
	bw
):
	# Add block to blockchain
	blockchain.add_block(bw)
	
	# Create reward
	indexes.sources[bw.hash] = [bw.block.issuer, BLOCK_REWARD]
	if bw.block.issuer in indexes.pubkeys:
		indexes.pubkeys[bw.block.issuer]["amount"] += BLOCK_REWARD
	else:
		indexes.init_pubkey(bw.block.issuer, amount=1)
	
	# Apply txs
	if "tx" in bw.block.content:
		for txw in bw.block.content["tx"]:
			
			for txi in txw.tx.inputs:
				txo = indexes.sources.pop(txi.hash)
				indexes.pubkeys[txo[0]]["amount"] -= txo[1]
			
			i = 0
			for txo in txw.tx.outputs:
				indexes.sources[HASH_FUNC(bw.hash + varint.encode(i))] = txo.to_tuple()
				if txo.pubkey in indexes.pubkeys:
					indexes.pubkeys[txo.pubkey]["amount"] += txo.amount
				else:
					indexes.init_pubkey(txo.pubkey, amount=txo.amount)
				i += 1
	
	print("Applied block {}".format(bw.human_readable_id()))

def revert_last_block(
	blockchain,
	indexes
):
	# Remove block from blockchain
	bw = blockchain.remove_last_block()
	
	# Revert reward
	indexes.pubkeys[bw.block.issuer]["amount"] -= BLOCK_REWARD
	indexes.sources.pop(bw.hash)
	
	# Revert txs
	if "tx" in bw.block.content:
		for txw in bw.block.content["tx"]:
			
			i = 0
			for txo in txw.tx.outputs:
				indexes.sources.pop(HASH_FUNC(bw.hash + varint.encode(i)))
				indexes.pubkeys[txo.pubkey]["amount"] -= txo.amount
				i += 1
			
			for txi in txw.tx.inputs:
				indexes.sources[txi.hash] = [txw.tx.issuer, txi.amount]
				indexes.pubkeys[txw.tx.issuer]["amount"] += txi.amount
	
	print("Reverted block {}".format(bw.human_readable_id()))
