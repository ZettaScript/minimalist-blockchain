#!/usr/bin/env python3

from constants import *
import utils
import global_valid, apply
from block import Block, BlockWrapper
from blockchain import Blockchain
from indexes import Indexes
from network import Client, Server
from pool import Pool
from pow import Pow

import os, sys, time

if __name__ == "__main__":
	
	if "--help" in sys.argv or "help" in sys.argv:
		print("""Subcommands:
  clean    remove data files
  genesis  generate genesis block
  start    start node

Options:
  -h <host>  (public) Default: <your-local-ip>
  -p <port>  (public) Default: 58753
  -H <host>  (bind) Default: ""
  -P <port>  (bind) Default: 58753
  -d <path>  Data directory. Default: "."
""")
		exit()
	
	# Get directory
	DIR = os.path.expanduser(utils.getargv("-d", "."))
	if DIR != "" and DIR[len(DIR)-1] == "/":
		DIR = DIR[:len(DIR)-1] # Remove last slash
	os.makedirs(DIR, exist_ok=True)
	
	BLOCKCHAIN_FILENAME = "{}/{}".format(DIR, BLOCKCHAIN_FILENAME)
	INDEXES_FILENAME = "{}/{}".format(DIR, INDEXES_FILENAME)
	PEERLIST_FILENAME = "{}/{}".format(DIR, PEERLIST_FILENAME)
	NETWORK_HOST_BIND = (utils.getargv("-H", NETWORK_HOST_BIND[0]), int(utils.getargv("-P", NETWORK_HOST_BIND[1])))
	NETWORK_HOST_PUBLIC = (utils.getargv("-h", NETWORK_HOST_PUBLIC[0]), int(utils.getargv("-p", NETWORK_HOST_PUBLIC[1])))
	
	if "clean" in sys.argv:
		try:
			os.remove(BLOCKCHAIN_FILENAME)
		except:
			pass
		try:
			os.remove(INDEXES_FILENAME)
		except:
			pass
	
	# Load data
	privkey = utils.prompt_privkey()
	blockchain = Blockchain(filename=BLOCKCHAIN_FILENAME)
	indexes = Indexes(filename=INDEXES_FILENAME)
	pow = Pow(privkey)
	pool = Pool(blockchain, indexes)
	peers = utils.read_peerlist(filename=PEERLIST_FILENAME)
	client = Client(blockchain, indexes, pool, peers, privkey, host_public=NETWORK_HOST_PUBLIC)
	
	try:
		blockchain.load()
	except:
		print("Loaded blockchain: empty")
	try:
		indexes.load()
	except:
		print("Loaded indexes: empty")
	
	if "genesis" in sys.argv:
		# Create genesis block
		block = Block(
			{},
			privkey.vk,
			0,
			GENESIS_BLOCK_PREVIOUS_HASH
		)
		
		# PoW genesis block
		pow.start(block)
		bw = pow.wait()
		
		# Verify block
		bw.check_local_validity()
		global_valid.check_global_validity(blockchain, indexes, bw)
		
		# Apply block
		apply.apply_block(blockchain, indexes, bw)
	
	elif len(blockchain.block_index) == 0:
		client.get_genesis_block()
	
	if "start" in sys.argv:
		server = Server(blockchain, indexes, pool, peers, privkey, host_bind=NETWORK_HOST_BIND, host_public=NETWORK_HOST_PUBLIC)
		
		server.start()
		client.start()
		
		try:
			while True:
				pool.update()
				
				if not pow.thread:
					docs = []
					for doc in pool.docs:
						try:
							global_valid.check_tx_global_validity(indexes, pool.docs[doc])
						except AssertionError:
							continue
						docs.append(pool.docs[doc])
					
					pow.start(Block(
						{"tx": docs} if len(docs) > 0 else {},
						privkey.vk,
						len(blockchain.block_index),
						blockchain.block_index[len(blockchain.block_index)-1].hash
					))
					pool.docs_changed = False
				
				bw = pow.get_state()
				if bw:
					with pool.docs_unchecked_lock:
						pool.blocks_unchecked[bw.hash] = bw
				
				elif pow.thread and pool.docs_changed:
					pow.stop()
				
				time.sleep(MAIN_LOOP_SLEEP)
				
		except KeyboardInterrupt:
			pass
		
		server.loop = False
		client.loop = False
		server.join()
		client.join()
	
	# Save data
	blockchain.save()
	indexes.save()
