#!/usr/bin/env python3

from constants import *

import math, getpass
from libnacl.sign import Verifier
from duniterpy.key import SigningKey, PublicKey

import sys

def leading_zeros(h):
	z = 0
	for b in h:
		if b == 0:
			z += 8
		else:
			z += 7-math.floor(math.log2(b))
			break
	return z

def is_valid_zeros(h, d):
	return leading_zeros(h) >= d

def verify_sign(raw):
	pubkey = raw[:PUBKEY_SIZE]
	return Verifier(pubkey.hex()).verify(raw[PUBKEY_SIZE:]), pubkey

def read_peerlist(filename=PEERLIST_FILENAME):
	f = open(filename, "r")
	result = []
	while True:
		line = f.readline()
		if line == "":
			break
		if line in ["\n", "\r", "\r\n", "\n\r"]:
			continue
		pre = line.replace("\n","").replace("\r","").split(":")
		result.append((pre[0], int(pre[1])))
	
	print("Loaded {} peers".format(len(result)))
	return result

def prompt_privkey():
	return SigningKey.from_credentials(getpass.getpass("Key: "), getpass.getpass("Salt: "))

def getargv(arg, default="", n=1, args=sys.argv):
	if arg in args and len(args) > args.index(arg)+n:
		return args[args.index(arg)+n]
	else:
		return default
