#!/usr/bin/env python3

from constants import *

def check_tx_global_validity(
	indexes,
	txw
):
	assert txw.tx.issuer in indexes.pubkeys , "Unknown issuer"
	assert len(txw.tx.inputs) > 0 , "Empty tx"
	amount = 0
	for txi in txw.tx.inputs:
		assert txi.hash in indexes.sources , "Unknown input"
		assert indexes.sources[txi.hash][0] == txw.tx.issuer , "Unauthorized input"
		assert indexes.sources[txi.hash][1] == txi.amount , "Invalid amount"
		amount += indexes.sources[txi.hash][1]
	for txo in txw.tx.outputs:
		assert txo.amount > 0 , "Null output"
		amount -= txo.amount
	assert amount == 0 , "Non-null operation"
	
	return True

def check_global_validity(
	blockchain,
	indexes,
	bw
):
	try:
		# Check number
		assert bw.block.number == len(blockchain.block_index) , "Invalid number"
		
		# Genesis block
		if bw.block.number == 0:
			
			# Check previous_hash
			assert bw.block.previous_hash == GENESIS_BLOCK_PREVIOUS_HASH , "Invalid previous_hash"
		
		# Other blocks
		else:
			
			# Check previous_hash
			assert bw.block.previous_hash == blockchain.block_index[bw.block.number-1].hash , "Invalid previous_hash"
		
		# Check txs
		if "tx" in bw.block.content:
			for txw in bw.block.content["tx"]:
				assert check_tx_global_validity(indexes, txw)
	
	except AssertionError:
		return False
	
	return True
